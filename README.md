# **AF2BB** - AutoFeedback Tool

Process marks from [**AutoFeedback**](https://gitlab.com/autofeedback/autofeedback-webapp) submissions - check if late, calculate late days (excluding weekends) and adjust marks for late - and format for import into **Blackboard**.

## **Requirements**

1. Python 3.9+

   To install Python, use the package manager (Linux) - e.g. on Ubuntu:

   ```bash
   sudo apt install python3
   ```

   Alternatively, [download](https://www.python.org/downloads/) python from *python.org* (any OS).

2. Python libraries: `pandas`, `numpy`

   For `pandas` and `numpy`:

   ```python
   python -m pip install pandas numpy
   ```

   or using `requirements.txt`:

   ```python
   python -m pip install -r requirements.txt
   ```

## **How to use**

1. (Optional) Set the names for the directories with the CSV files in [*config.py*](config.py):

   `AF_CSV_FOLDER`: directory with the downloaded from *AutoFeedback* CSV files

   - default directory name: [*CSV_exported_from_AF*](CSV_exported_from_AF)
  
   `BB_CSV_FOLDER`: directory with the generated CSV files, formatted for importing into *Blackboard*

   - default directory name: [*CSV_import_into_BB*](CSV_import_into_BB)

2. Download the CSV files with the marks from *AutoFeedback* and save them in the directory `AF_CSV_FOLDER`, as set in [*config.py*](config.py)

   - default directory name: [*CSV_exported_from_AF*](CSV_exported_from_AF)

3. Set the assessment information in [*data.csv*](data.csv)

4. Run `main.py`, and select from the options for which week / unit to process and adjust marks:

   ```python
   python main.py
   ```

   - Alternatively, if you want to also create a CSV file with the details for *lateness* and *achieved* vs *adjusted marks*, use argument `--details`:

   ```python
   python main.py --details
   ```

   Generated CSV files, prepared and formatted for importing into *Blackboard* are stored in the directory `BB_CSV_FOLDER`, as set in [*config.py*](config.py) (default name: [*CSV_import_into_BB*](CSV_import_into_BB)).
