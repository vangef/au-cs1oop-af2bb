# data file needs to be set up with the information about each week/assessment
# contains info about weekly labs (week number, assessment code from AF, assessment deadline from BB, and column name in BB for assessment)
DATA_FILE = 'data.csv'
AF_CSV_FOLDER = 'CSV_exported_from_AF'  # directory with the downloaded AF marks csv files 
BB_CSV_FOLDER = 'CSV_import_into_BB'  # directory to store csv with processed / adjusted marks, dir will be created if not exists

# column names from AF csv not needed for dataframe
UNWANTED_COLUMNS = ['Module ID','Module Name','Assessment ID','Assessment Title','Submission ID','Author Name','Updated at','Attempt','Model Solution Version','Outdated','Achievable Marks','ZIP Filename','ZIP SHA-256','Passed','Failed','Errored','Skipped','Missing']
