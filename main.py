import os, sys
import pandas as pd
from datetime import datetime
from config import DATA_FILE, UNWANTED_COLUMNS, BB_CSV_FOLDER
from f_main import display_menu_select_week, find_file_by_code, save_CSV_for_BB, save_detailed_csv
from f_apply2df import mark_late, calc_late_busdays, calc_adjusted_marks, email_to_id

def main():
    data_csv = pd.read_csv(DATA_FILE)
    data_df = pd.DataFrame(data_csv)

    selected_week = display_menu_select_week(data_df)  # display menu of available assessments and select week to process marks
    assessment_code = data_df.loc[data_df['week_number'] == selected_week]['assessment_AF_code'].values[0]
    assessment_deadline_str = data_df.loc[data_df['week_number'] == selected_week]['assessment_BB_deadline'].values[0]
    assessment_deadline = datetime.strptime(assessment_deadline_str, '%d/%m/%Y %H:%M')
    bb_col_title = data_df.loc[data_df['week_number'] == selected_week]['bb_column_name'].values[0]  # (long) column title in bb for the assessment

    AF_csv_file = find_file_by_code(assessment_code)  # find csv file in dir AF_CSV_FOLDER from the assessment code
    if AF_csv_file is not None:
        AF_csv = pd.read_csv(AF_csv_file)
        AF_df = pd.DataFrame(AF_csv)
    else:
        exit(f'Cannot find a CSV file matching the given assessment code in the directory set in config - check data.csv and config.py')

    df = AF_df.drop(columns=UNWANTED_COLUMNS)  # clear dataframe from unwanted columns used in AF csv
      
    df['_Late_'] = df.apply(mark_late, args=(assessment_deadline,), axis=1)  # mark as late if submission after deadline
    df['late_days'] = df.apply(calc_late_busdays, args=(assessment_deadline,), axis=1)  # calculate how many (business) days late
    df[bb_col_title] = df.apply(calc_adjusted_marks, axis=1)  # add column (name same as in BB) with adjusted marks 
    df['Student ID'] = df.apply(email_to_id, axis=1)  # add column with student id
    df = df[df['Student ID'] != 'STAFF']  # remove AF submissions with ID 'STAFF'

    # save detailed csv with adjusted marks for lateness - used for checking if marks are correctly adjusted - usage: python main.py --details
    if len(sys.argv) >= 2 and sys.argv[1] == '--details':
        save_detailed_csv(df, selected_week)
    
    grouped_df = df.groupby('Student ID')
    max_marks = grouped_df.max(bb_col_title)  # find the max adjusted mark from the grouped by student id df
    df_for_bb = max_marks.drop(columns=['Late','_Late_', 'Achieved Marks'])  # clear not needed columns from df / prepare for bb csv

    print(f'[INFO] Marks processed and late adjusted for Week {selected_week} - BB column: "{bb_col_title}"')
    output_filename = bb_col_title.split('[')[0].strip().replace(':','').replace('/','')
    output_csv = os.path.join(BB_CSV_FOLDER, f'{output_filename}.csv')
    save_CSV_for_BB(df_for_bb, output_csv)

if __name__ == '__main__':
    main()

