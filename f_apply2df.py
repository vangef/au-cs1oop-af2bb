### dataframe functions - used to process submission data with 'apply' on df ###

from datetime import datetime
import pandas as pd
import numpy as np

# function to mark submissions as Late manually because AF doesn't always have deadline set and/or submissions are not marked 'late'
def mark_late(row, the_deadline):
    created_at = datetime.strptime(row['Created at'], '%Y-%m-%d %H:%M:%S')
    return 1 if created_at > the_deadline else 0

# calculate lateness if AF submission is marked late, using its 'created_at' date/time and the assessment deadline from bb
# returns difference in days late excluding weekends and takes into account the exact times of deadline and submission 
def calc_late_busdays(row, the_deadline):
    if row['_Late_'] == 1:
        created_at = pd.to_datetime(row['Created at'])
        created_at_date = created_at.date()
        created_at_H = created_at.hour
        created_at_M = created_at.minute

        deadline = pd.to_datetime(the_deadline)
        deadline_date = deadline.date()
        deadline_H = deadline.hour
        deadline_M = deadline.minute

        late_busdays = np.busday_count(deadline_date, created_at_date)  # does not include end date in calcuation - will add 1 before returning late_days @refA
        late_bushours = late_busdays * 24
        late_busmins = late_bushours * 60
        late_busmins = late_busmins - (deadline_H*60) - deadline_M  # subtract hours/mins since midnight for deadline day/time 
        if created_at.isoweekday() <= 5:  # if submission created on a weekday, add the hours/mins passed since midnight
            late_busmins = late_busmins + (created_at_H*60) + created_at_M
        late_days = int(late_busmins/60/24) + 1  # adding 1 day to adjust the calculation from np.busday_count() @refA
        return late_days
    else:
        return 'n/a'

# calculate mark after late penalty applied - 10% reduction / day late, max days late 5 (incl.), 0 marks if 6+ days late
def calc_adjusted_marks(row):
    adj_mark = row['Achieved Marks']
    if row['_Late_'] == 1 and row['late_days'] <= 5:
        adj_mark = adj_mark * (1 - 0.1*(row['late_days']))
    if row['_Late_'] == 1 and row['late_days'] > 5:
        adj_mark = 0
    return adj_mark

# convert 'author email' from AF to student id (or to 'STAFF' if id is not 9 digits/chars long OR has '.' in id)
def email_to_id(row):
    id = row['Author Email'].split('@')[0]
    return id if len(id) == 9 and '.' not in id else 'STAFF'

