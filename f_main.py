### main script functions ###

import os
import pandas as pd
from datetime import datetime
from config import AF_CSV_FOLDER, BB_CSV_FOLDER

def find_file_by_code(code):
    try:
        for fname in os.listdir(AF_CSV_FOLDER):
            if code in fname:
                return os.path.join(AF_CSV_FOLDER, fname)
        return None
    except FileNotFoundError as e:
        exit(f'Cannot find the directory with the CSV files downloaded from AF - check config.py - error: {e}')

def display_menu_select_week(df: pd.DataFrame):
    print('Week number - Assessment name on BB')    
    for index, row in df.iterrows():
        title = row['bb_column_name'].split("[")[0]
        print(f"{row['week_number']} - {title}")
    
    try:
        selected_week = int(input('\nEnter week number to process marks from AF for import into BB: '))
    except ValueError as e:
        exit(f'Not a valid option - {e}')

    if selected_week not in df['week_number'].values:
        exit('Selected week not available')
    return selected_week

def save_CSV_for_BB(df: pd.DataFrame, file):
    os.makedirs(BB_CSV_FOLDER, exist_ok=True)
    try:
        df.to_csv(file)
        print(f'[OK] Saved in file ./{file}')
    except FileNotFoundError as e:
        print(f'[ERROR] Could not save file for BB import - possible invalid filename character in assessment name (check data.csv)')

def save_detailed_csv(df: pd.DataFrame, week):
    try:
        fname = f'Week {week} - detailed adjusted.csv'
        df.to_csv(fname)
        print(f'... also saving csv with detailed adjusted marks: {fname}')
    except PermissionError as e:  # if file is already opened, append current time to filename and then save
        fname = f'Week {week} - detailed adjusted {datetime.now().strftime("%H%M%S")}.csv'
        df.to_csv(fname)
        print(f'... also saving csv with detailed adjusted marks: {fname}')

